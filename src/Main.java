import java.io.IOException;
import java.util.*;

public class Main {

    private static final int n = (int) (Math.random() * 1000) + 1000;


    public static void main(String[] args){
        System.out.flush();

        List<Integer> d = new ArrayList<>();
        for (int i = 0; i < n; i++) d.add((int) (Math.random() * 5000));

        System.out.println(d);
        qSort(d);
        System.out.println(d);

        char[] q = new char[n];
        for (int i = 0; i < n; i++) q[i] = (char) ((Math.random() * 26) + 97);

        System.out.println(new String(q));
        int enty = getFirstEntry(new String(q), "qwerretetqwe   ");

        if (enty == -1) main(null);
        else System.out.println("ПОЗИЦИЯ: " + enty);


    }

    //сортировк-а Хоара
    private static void qSort(List<Integer> array) {
        Random rand = new Random();
        int n = array.size();
        int i = 0;
        int j = n - 1;
        int x = array.get(rand.nextInt(n));

        while (i <= j) {
            while (array.get(i) < x) {
                i++;
            }
            // System.out.println("X:" + x);
            //  System.out.println("I:" + i);
            while (array.get(j) > x) {
                j--;
            }
            //  System.out.println("J:" + j);
            if (i <= j) {
                Collections.swap(array, i, j);
                i++;
                j--;
            }
        }
        if (j > 0) {
            qSort(array.subList(0, j + 1));
        }
        if (i < n) {
            qSort(array.subList(i, n));
        }
    }

    //Алгоритм Боуера-Мура

    private static int getFirstEntry(String source, String template) {

        int[] d1 = makeD1(template.toCharArray());

        int[] d2 = makeD2(template.toCharArray());

        int i = template.length() - 1;
        while (i < source.length()) {
            int j = template.length() - 1;
            while (j >= 0 && (source.charAt(i) == template.charAt(j))) {
                i--;
                j--;
            }
            if (j < 0)
                return (i + 1);
            i += Math.max(d1[source.charAt(i)], d2[j]);
        }
        return -1;
    }


    private static int[] makeD1(char[] pat) {
        int[] table = new int[255];
        for (int i = 0; i < 255; i++)
            table[i] = pat.length;
        for (int i = 0; i < pat.length - 1; i++)
            table[pat[i]] = pat.length - 1 - i;
        return table;
    }

    private static int[] makeD2(char[] pat) {
        int[] delta2 = new int[pat.length];
        int p;
        int last_prefix_index = pat.length - 1;
        for (p = pat.length - 1; p >= 0; p--) {
            if (isPrefix(pat, p + 1))
                last_prefix_index = p + 1;
            delta2[p] = last_prefix_index + (pat.length - 1 - p);
        }
        for (p = 0; p < pat.length - 1; p++) {
            int slen = suffix_length(pat, p);
            if (pat[p - slen] != pat[pat.length - 1 - slen])
                delta2[pat.length - 1 - slen] = pat.length - 1 - p + slen;
        }
        return delta2;
    }


    private static boolean isPrefix(char[] word, int pos) {
        int suffixlen = word.length - pos;
        for (int i = 0; i < suffixlen; i++)
            if (word[i] != word[pos + i])
                return false;
        return true;
    }

    private static int suffix_length(char[] word, int pos) {
        int i;
        for (i = 0; ((word[pos - i] == word[word.length - 1 - i]) &&
                (i < pos)); i++) {
        }
        return i;
    }

}
